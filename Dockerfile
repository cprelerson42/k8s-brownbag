FROM python
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
RUN chown -R 24601:24601 /app
USER 24601:24601
CMD ["/app/app.py"]