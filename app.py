#! /usr/bin/env python
from flask import Flask
from flask import request
import redis
import os
import datetime

redis_host = os.getenv('REDIS_HOST')
redis_port = os.getenv('REDIS_PORT')
redis_pass = os.getenv('REDIS_PASS')
redis_log =  os.getenv('LOGFILE')
r = redis.Redis(host = redis_host,
                port = redis_port)


def init_redis():
    r.set('count', 0)

def log_event(event_string):
    ct = datetime.datetime.now()
    log_string = f'{ct}::{event_string}\n'
    print (log_string)
    with open(redis_log, 'a+') as logfile:
        logfile.write(log_string)

app = Flask(__name__)

@app.route('/')
def home():
    log_event(f"{request.remote_addr} -- Welcome home")
    return f"Current count: {int(r.get('count'))}"


@app.route('/add_count')
def add_count():
    count = r.get('count')
    log_event(f"{request.remote_addr} -- Add to count")
    r.set('count', int(count) + 1)
    return "Count updated"

@app.route('/reset')
def reset_count():
    print(redis_pass)
    passwd = request.args.get('pass')
    print(passwd)
    if passwd == redis_pass:
        log_event(f"{request.remote_addr} -- User authorized")
        init_redis()
        log_event(f"{request.remote_addr} -- Instance reset")
        return "Redis instance reset"
    else:
        log_event(f"{request.remote_addr} -- Unauthorized reset attempt")
        return "Unauthorized attempt", 401


if __name__ == '__main__':
    count = r.get('count')
    if not count:
        init_redis()
    app.run(host='0.0.0.0')